package com.bezkoder.spring.datajpa.exception;

public class MyException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8419679913954824880L;

	public MyException(String message) {
		super(message);
	}
	
	

}
