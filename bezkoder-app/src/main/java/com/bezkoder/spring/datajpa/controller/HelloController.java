package com.bezkoder.spring.datajpa.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.datajpa.exception.MyException;

@RestController
public class HelloController {
	
	private List<MyObj> objects = new ArrayList<>();
	
	private Logger LOG = LoggerFactory.getLogger(HelloController.class);
	
	@RequestMapping("/threads")
	public String threads() {
		ExecutorService service = Executors.newFixedThreadPool(10, new CustomizableThreadFactory("findme-"));
		for(int i=0;i<=10;i++) {
			
			int ii = i;
			service.execute(new Runnable() {
				
				@Override
				public void run() {
					
					LOG.debug("The current counter is {} ",+ii);
				}
			});
		}
		return "Greetings from Spring Boot Sameer!";
	}
	
	@RequestMapping("/objects")
	public String objects() {
		for(int x=0;x<=1000;x++) {
			String message = "ababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierfababorjoiejfoierf";
			objects.add(new MyObj(message));
		}
		return "Greetings from Spring Boot Rachit!";
	}
	
	@RequestMapping("/throwexception")
	public String throwexception() throws Exception {
		
		throw new MyException("This exception is generated purposefully to test profiling the Java App");
		
	}
}
