package com.bezkoder.spring.datajpa.controller;


public class MyObj {
	private String message;

	public MyObj(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
